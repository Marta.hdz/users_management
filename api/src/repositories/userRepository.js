const postUserQuery = ({name, surname, email, password, country, phone, postalCode}) => {
  return `INSERT INTO users.users_info (name, surname, email, password, country, phone, postal_code)`+
  `values ('${name}', '${surname}', '${email}', '${password}', '${country}', ${phone}, ${postalCode});`+
  `SELECT * FROM users.users_info WHERE email = '${email}'`
}

const getUserQuery = (email) => {
  return `SELECT * FROM users.users_info WHERE email = '${email}'`
}

const putUserQuery = ({name, surname, email, password, country, phone, postalCode}) => {
  return `UPDATE users.users_info SET name ='${name}', surname ='${surname}', password ='${password}',` +
  `country = '${country}', phone = ${phone}, postal_code = ${postalCode} WHERE email = '${email}';` +
  `SELECT * FROM users.users_info WHERE email = '${email}'`
}

const deleteUserQuery = (email) => {
  return `DELETE FROM users.users_info WHERE email = '${email}'`
}

export {postUserQuery,  getUserQuery, putUserQuery, deleteUserQuery}
