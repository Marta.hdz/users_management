export default {
  environment: 'production',
  bodyLimit: '300kb',
  port: 3000,
  saltRounds: 10,
  jwtTokenTime: 60 * 60 *3,
  jwtTokenSecret: '@S3cret-jWt-t0K3n' ,
  db : {
    host: 'db',
    user: 'root',
    password: 'tester123',
  }
}
