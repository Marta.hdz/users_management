import mysql from 'mysql'

const mysqlAdapter = () => {
  const start = config => mysql.createConnection(Object.assign({}, config.db, {multipleStatements: true}))
  const end = conn => conn.end()

  const execute = (query, conn) => {
    return new Promise(function(resolve, reject) {
      if(!query || query.trim().length === 0) {
        reject('No query provided')
      } else {
        conn.query(query, function(err, result) {
          if(err) {
            console.log(err)
            reject(err)
          } else {
            resolve(result)
          }
        })
      }
    })
  }
  return {start, end, execute}
}

export default mysqlAdapter()
