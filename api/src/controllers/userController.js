import bcrypt from 'bcryptjs'
import mysql from '../adapters/mysql.js'
import {postUserModel, getUserModel, putUserModel, deleteUserModel} from '../models/userModel.js'
import {sendUnauthorizedResponse} from '../utils/responses.js'

const postUserController = (req, res, next, config) => {
  const {name, surname, email, password, country, phone, postalCode} = req.body
  const conn = mysql.start(config)

  bcrypt
  .hash(password, config.saltRounds)
  .then(hash => postUserModel({name, surname, email, password: hash, country, phone, postalCode, conn}))
  .then(user => {
      const result = {
        _data: {
          message: 'User created',
          newUser: user
        }
      }
      return next(result)
    })
    .catch(err => {
      console.log(err)
      res.status(500).json(err)
    })
    .finally(()=> mysql.end(conn))
}

const postLoginController = (req, res, next, config) => {
  const {email, password} = req.body
  const conn = mysql.start(config)

  return getUserModel({email, conn})
    .then(user => {
      if (user && user.length <1) {return sendUnauthorizedResponse(res, 'Unauthorized')}
        return bcrypt
          .compare(password, user[0].password)
          .then((verified) => {
              if (!verified) {return sendUnauthorizedResponse(res, 'Unauthorized')}

              const {email, name} = user[0]
              const result = {
                _data: {auth: true, email, name}
              };

              next(result)
          })

    })
    .catch(err => {
      console.log(err)
      res.status(500).json(err)
    })
    .finally(()=> mysql.end(conn))
}

const putUserController = (req, res, next, config) => {
  const {name, surname, email, password, newPassword, country, phone, postalCode} = req.body
  const conn = mysql.start(config)

  return getUserModel({email, conn})
    .then(user => {
      if (user && user.length <1) {next({message: 'Invalid oldPassword'})}

      return bcrypt
        .compare(password, user[0].password)
        .then((verified) => {
            if (!verified) {next({message: 'Invalid oldPassword'})}

            return bcrypt
            .hash(newPassword, config.saltRounds)
            .then(hash => putUserModel({name, surname, email, password: hash, country, phone, postalCode, conn}))
                .then(modifiedUser => {
                  const result = {
                    _data: {
                        message: 'User modified',
                        newUser: modifiedUser
                    }
                  }
                  next(result)
                })
          })
    })
    .catch(err => {
      console.log(err)
      res.status(500).json(err)
    })
    .finally(()=> mysql.end(conn))
}

const deleteUserController  = (req, res, next, config) => {
  const {email} = req.body
  const conn = mysql.start(config)

  return deleteUserModel({email, conn})
    .then(res => {
      const result = {
        _data: {
            message: 'User deleted'
        }
      }
      next(result)
    })
    .catch(err => {
      console.log(err)
      res.status(500).json(err)
    })
    .finally(()=> mysql.end(conn))

}

export {postUserController, postLoginController, putUserController, deleteUserController}
